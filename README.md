# mylogin_interceptor

登录模块拦截器

1. 方法上增加注解

   @LoginVerifyAnnotation

2. 获取拦截器的校验结果

   Object attribute = request.getAttribute(LoginInterceptorConstant.USER_INFO);

3. 将拦截器的校验结果解析

   LoginVerifyResult loginVerifyResult = LoginVerifyResult.parseObject(attribute);

4. 判断结果是否成功

   if(loginVerifyResult.isSuccess()){

5. 如果成功获取用户id

   Long accountId = loginVerifyResult.getAccountId();

代码示例

    @LoginVerifyAnnotation
    @PostMapping("/testLoginInterceptorController")
    public ResponseResultVO<String> testLoginInterceptorController(HttpServletRequest request){
        Object attribute = request.getAttribute(LoginInterceptorConstant.USER_INFO);
        logger.info("-- TestLoginInterceptorController testLoginInterceptorController --,attribute:{}",attribute);
        LoginVerifyResult loginVerifyResult = LoginVerifyResult.parseObject(attribute);
        if(loginVerifyResult.isSuccess()){
            Long accountId = loginVerifyResult.getAccountId();
            logger.info("-- TestLoginInterceptorController testLoginInterceptorController --,accountId:{}",accountId);
        }
        return ResponseUtils.ok(null);
    }

