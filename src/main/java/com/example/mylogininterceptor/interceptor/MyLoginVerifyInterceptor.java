package com.example.mylogininterceptor.interceptor;

import com.example.mylogininterceptor.annotation.LoginVerifyAnnotation;
import com.example.mylogininterceptor.common.constants.LoginInterceptorConstant;
import com.example.mylogininterceptor.common.utils.LoginVerifyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * =======================================================
 *
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className MyLoginVerifyInterceptor
 * @description 执行验证的方法
 * @date 2021/10/17 11:04 上午
 */
@Component
public class MyLoginVerifyInterceptor implements HandlerInterceptor {

    private final static Logger logger = LoggerFactory.getLogger(MyLoginVerifyInterceptor.class);

    private static String doMain = "http://localhost:8080";
    private static String cluster = "null";
    private static String path = "/verifyToken";
    private static String verify = doMain+path;

    /**
     * 初始化请求地址和集群名
     * @param host 域名
     * @param name 集群名
     */
    public static void init(String host,String name){
        doMain = host;
        cluster = name;
        verify = host+path;
    }

    /**
     * 校验是否登录的拦截器
     *
     * @param request  request
     * @param response response
     * @param handler  hander
     * @return 是否通过
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String tokenS = "",uuid = "";
        try {
            if (!(handler instanceof HandlerMethod)) {
                return true;
            }
            //给每次请求生成一个唯一的id志，方便异常查询
            uuid = UUID.randomUUID().toString().replaceAll("-","");
            request.setAttribute(LoginInterceptorConstant.UUID,uuid);
            LoginVerifyAnnotation annotation = ((HandlerMethod) handler).getMethod().getAnnotation(LoginVerifyAnnotation.class);
            if (annotation != null && annotation.verify()) {
                //获取要校验的token
                tokenS = LoginVerifyUtils.getTokenS(request);
                if (StringUtils.isBlank(tokenS)) {
                    return true;
                }
                //发送http请求校验参数
                httpPost(request,tokenS,uuid);
            }
            return true;
        } catch (Exception e) {
            String format = String.format("-- MyLoginVerifyInterceptor preHandle exception --,uuid:%s,tokenS:%s", uuid, tokenS);
            logger.error(format,e);
            throw new RuntimeException(format);
        }
    }


    /**
     * 发送http请求
     * @param request 请求对象
     * @param tokenS 令牌
     * @param uuid 唯一id
     */
    private void httpPost(HttpServletRequest request, String tokenS, String uuid) {
        // 创建 HttpClient 客户端
        CloseableHttpClient httpClient = HttpClients.createDefault();
        // 创建 HttpPost 请求
        HttpPost httpPost = new HttpPost(verify);
        // 创建 HttpPost 参数
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(LoginInterceptorConstant.TOKEN, tokenS));
        params.add(new BasicNameValuePair(LoginInterceptorConstant.UUID, uuid));
        CloseableHttpResponse httpResponse = null;
        try {
            // 设置 HttpPost 参数
            httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            // 输出请求结果
            request.setAttribute(LoginInterceptorConstant.USER_INFO,EntityUtils.toString(httpEntity));
        } catch (Exception e) {
            String format = "-- MyLoginVerifyInterceptor httpPost exception --";
            logger.error(format,e);
            throw new RuntimeException(format);
        }
        // 无论如何必须关闭连接
        finally {
            if (httpResponse != null) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
