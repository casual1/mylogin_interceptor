package com.example.mylogininterceptor.common.utils;

import com.example.mylogininterceptor.common.constants.LoginInterceptorConstant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className LoginVerifyUtils
 * @description 拦截器工具类
 * @date 2021/11/14 9:39 下午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
public class LoginVerifyUtils {

    private final static Logger logger = LoggerFactory.getLogger(LoginVerifyUtils.class);

    /**
     * @className LoginVerifyUtils
     * @description 获取token令牌
     * @date 2021/11/14 9:42 下午
     * @param request request对象
     * @return java.lang.String
     * @author liwenjie
     */
    public static String getTokenS(HttpServletRequest request){
        String tokenS = "";
        StringBuilder stringBuilder = new StringBuilder(1000);
        Object uuid = request.getAttribute(LoginInterceptorConstant.UUID);
        stringBuilder.append(LoginInterceptorConstant.UUID).append(":").append(uuid).append(",");
        String requestURI = request.getRequestURI();
        stringBuilder.append("requestURI:").append(requestURI).append(",");
        String method = request.getMethod();
        stringBuilder.append("method:").append(method).append(",");
        Enumeration<String> headerNames = request.getHeaderNames();
        stringBuilder.append("headers:{");
        while (headerNames.hasMoreElements()) {
            String s = headerNames.nextElement();
            String header = request.getHeader(s);
            stringBuilder.append(s).append(":").append(header).append(",");
            if (StringUtils.isBlank(tokenS) && LoginInterceptorConstant.TOKEN.equals(s)) {
                //在header中存在
                tokenS = header;
            } else if (StringUtils.isBlank(tokenS) && LoginInterceptorConstant.COOKIE.equals(s) && StringUtils.isNotBlank(header)) {
                //cookie中存在
                String[] split = header.split(";");
                for (String s1 : split) {
                    if (StringUtils.isNotBlank(s1)) {
                        String[] split1 = s1.split("=");
                        if (split1.length == 2 && LoginInterceptorConstant.TOKEN.equals(split1[0])) {
                            tokenS = split1[1];
                        }
                    }
                }
            }
        }
        stringBuilder.append("},").append(LoginInterceptorConstant.TOKEN).append(":").append(tokenS);
        logger.info("-- LoginVerifyUtils getTokenS --,param:{}",stringBuilder);
        return tokenS;
    }
}
