package com.example.mylogininterceptor.common.constants;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className Constant
 * @description 常量类
 * @date 2021/10/17 11:37 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
public class LoginInterceptorConstant {

    public static final String USER_INFO = "user_info";
    public static final String TOKEN = "tokenS";
    public static final String UUID = "uuid";
    public static final String COOKIE = "cookie";
}
