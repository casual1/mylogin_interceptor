package com.example.mylogininterceptor.vo;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className LoginVerifyResult
 * @description 拦截器校验返回结果类
 * @date 2021/11/14 10:11 下午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
public class LoginVerifyResult {

    private static final int OK = 1;

    private int code;
    private String msg;
    private Long accountId;

    public static LoginVerifyResult parseObject(Object object){
        if(object == null || StringUtils.isBlank(object.toString()))
            return new LoginVerifyResult(-1,"入参为空",-1L);
        JSONObject jsonObject = JSONObject.parseObject(object.toString());
        if(jsonObject==null || !jsonObject.containsKey("code") || !jsonObject.containsKey("data"))
            return new LoginVerifyResult(-2,"参数解析失败",-1L);
        if(jsonObject.getInteger("code")!=OK)
            return new LoginVerifyResult(-3,jsonObject.getString("msg"),-1L);
        JSONObject data = JSONObject.parseObject(jsonObject.getString("data"));
        if(!data.containsKey("accountId") || data.getLong("accountId")<1)
            return new LoginVerifyResult(-4,"用户id解析失败",-1L);
        return new LoginVerifyResult(OK,"登录状态",data.getLong("accountId"));
    }

    public LoginVerifyResult(int code, String msg, Long accountId) {
        this.code = code;
        this.msg = msg;
        this.accountId = accountId;
    }

    public boolean isSuccess(){
        return this.code == OK;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Long getAccountId() {
        return accountId;
    }

    @Override
    public String toString() {
        return "LoginVerifyResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", accountId=" + accountId +
                '}';
    }
}
