package com.example.mylogininterceptor.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * =======================================================
 * @company 技术中心-中台技术部-共享平台组
 * @className LoginVerifyAnnotation
 * @description 判断是否需要验证登录状态的标志
 * @date 2021/10/17 10:49 上午
 * @author liwenjie
 * @version 0.0.1
 * =======================================================
 */
@Target({ElementType.FIELD, ElementType.METHOD}) //声明自定义的注解使用在方法上
@Retention(RetentionPolicy.RUNTIME)//注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在
public @interface LoginVerifyAnnotation {

    /**
     * 默认需要校验
     */
    boolean verify() default true;
}
